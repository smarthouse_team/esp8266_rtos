# ESP8266_RTOS #

Code for running on wireless ESP-XX module with ESP8266 chip onboard (no external MC needed). Allows to run standalone IoT device adopted to [Crouton Dashboard](http://crouton.mybluemix.net/crouton/gettingStarted).

### Dependencies ###

* Espressif [ESP8266 RTOS SDK](https://github.com/espressif/ESP8266_RTOS_SDK)
* [MQTT client with ESP8266 RTOS SDK](https://github.com/baoshi/ESP-RTOS-Paho)

### Planned features ###

* MQTT Client
* DS18B20 temperature sensor communication and printing results on dashboard
* Digital outputs controlled from dashboard (On/Off)
* PWM outputs controlled from dashboard (Slider)
* HTTP-server for initial board configuration
* RTC
* NTP Client