/*
 * user_main.h
 *
 *  Created on: 16 Aug 2016
 *      Author: Alexander Vorontsov
 */

#ifndef USER_USER_MAIN_H_
#define USER_USER_MAIN_H_

//Includes
#include <espressif/esp_common.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/portable.h"
#include "freertos/queue.h"


#include "MQTTESP8266.h"
#include "MQTTClient.h"
#include "MQTTPacket.h"

#include "crouton_dev.h"
//Types
typedef enum
{
	TASK_RUN,
	TASK_STOP
} taskSignal_t;

#endif /* USER_USER_MAIN_H_ */
