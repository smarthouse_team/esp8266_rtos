/*
 * user_main.c
 *
 *  Created on: 7 Aug 2016
 *      Author: Alexander Vorontsov
 */

#include "user_main.h"
#include "user_config.h"


//FreeRTOS variables
xQueueHandle mqttControlQueue, mqttRxQueue;


inline void wifiInit(void)
{
	wifi_set_opmode(STATION_MODE);

	struct station_config stConfig;

	sprintf(stConfig.ssid, "%s", WIFI_CLIENTSSID);
	sprintf(stConfig.password, "%s", WIFI_CLIENTPASSWORD);

//Should add some error checks later
	wifi_station_set_config_current(&stConfig);
	wifi_station_connect();
	wifi_station_dhcpc_start();
	while(wifi_station_get_connect_status() != STATION_GOT_IP);
	printf("ESP8266 in STA mode configured. \r\n");
}

// Callback when receiving subscribed message
LOCAL void ICACHE_FLASH_ATTR mqttTopicReceivedCb(MessageData* md)
{
	//Forward message data to MQTT task
	xQueueSend(mqttRxQueue,&md,0);
}

// Main task for user application
void taskMain(void *pvParameters)
{
	// 1s delay to boot up
	vTaskDelay(1000/portTICK_RATE_MS);
	taskSignal_t mqttSignal;
	while (1)
	{
		//Initialization in blocking mode. Will not return until gets IP address
		wifiInit();
		while(wifi_station_get_connect_status() == STATION_GOT_IP)
		{
			// WiFi connected properly and device has IP address. Allow MQTT task to run
//			printf("%i\n",system_get_free_heap_size());
			printf("WiFi connected. Now giving semaphore to MQTT task\n");
			mqttSignal = TASK_RUN;
			xQueueSend(mqttControlQueue,&mqttSignal,0);
			vTaskDelay(5000/portTICK_RATE_MS);
		}
		mqttSignal = TASK_STOP;
		xQueueSend(mqttControlQueue,&mqttSignal,0);
	}
}

// MQTT task
void taskMqtt(void *pvParameters)
{
	//MQTT buffers
	unsigned char *mqtt_txbuf = pvPortMalloc(MQTT_TXBUF_LEN);
	unsigned char *mqtt_rxbuf = pvPortMalloc(MQTT_RXBUF_LEN);

	//Other MQTT staff
	MQTTClient client = DefaultClient;
	struct Network network;
	char mqtt_client_id[20] = MQTT_CLIENT_ID;
	MQTTPacket_connectData data = MQTTPacket_connectData_initializer;

	taskSignal_t controlSignal;

	// Define Crouton device and its endpoints
   	croutonDevHandle_t deviceHandle;
   	croutonEndpointHandle_t pointOne;
	if (croutonDeviceCreate(&deviceHandle,DASHBRD_DEVNAME,DASHBRD_DEVDESCR,"good") == DEVICE_CREATE_OK)
		if (croutonEndpointAdd(&pointOne,&deviceHandle,"pointOne","Endpoint 1","someUnit",ENDPOINT_SIMPLE_TEXT) == DEVICE_CREATE_OK)
		{
			NewNetwork(&network);

			//Prepare MQTT data for connection to the server
			// Last will
			MQTTPacket_willOptions lwtOptions = MQTTPacket_willOptions_initializer;
            lwtOptions.topicName.cstring = croutonAppendTopic(deviceHandle.outboxTopic,"lwt");
            lwtOptions.message.cstring = ".";
            //Payload
            data.MQTTVersion = 3;
            data.clientID.cstring = mqtt_client_id;
            data.username.cstring = MQTT_USER;
            data.password.cstring = MQTT_PASS;
            data.keepAliveInterval = 10; // interval for PING message to be sent (seconds)
            data.cleansession = 0;
            data.willFlag = 1;
            data.will = lwtOptions;
            //Subscribe topics
            char *inboxDevInfo = croutonAppendTopic(deviceHandle.inboxTopic,"deviceInfo");


	while (1)
		{
		if(xQueueReceive(mqttControlQueue, &controlSignal, portMAX_DELAY))
			if (controlSignal == TASK_RUN)
			{
                printf("Trying to connect MQTT broker\n");
                if(ConnectNetwork(&network, MQTT_HOST, MQTT_PORT)>=0)
                {
                    printf("TCP connection established\n");

                    NewMQTTClient(&client, &network, 5000, mqtt_txbuf, MQTT_TXBUF_LEN, mqtt_rxbuf, MQTT_RXBUF_LEN);

                    if (MQTTConnect(&client, &data)==SUCCESS)
                        {
                            printf("Connected MQTT broker successfully\n");

            				//Should send deviceInfo immediately after connection
            				if (croutonSendDevInfo(&deviceHandle,&client) != CROUTON_OK) continue;

            				//Define inbox/deviceInfo Topic

            					if (MQTTSubscribe(&client, inboxDevInfo, QOS0, mqttTopicReceivedCb)==SUCCESS)
            						{
            							printf("Seccessfully subscribed to the topic \"%s\"\n", inboxDevInfo);
            						} else continue;

                            //Start mqtt/crouton maintenance routines
          					MessageData *msg; // Define where to read Rx queue
                                while (1)
                                {
                                	if (xQueueReceive(mqttControlQueue,&controlSignal,0))
                                	{
                                		if (controlSignal == TASK_STOP) {break;}
                                	}
                                    //Should do polling all the time in order to get new messages :(
                                    //Blocking function
                                    if (MQTTYield(&client, 200) == DISCONNECTED) {break;}

                                    while(xQueueReceive(mqttRxQueue,&msg,0))
                                    {
                                    	//Parse received message


                        				if (croutonSendDevInfo(&deviceHandle,&client) != CROUTON_OK) continue;
                                    }
                                    	// Give timeslot for other tasks
                                  //  taskYIELD();
                                    vTaskDelay(500/portTICK_RATE_MS);
                                }
                       }
                    else printf("Couldn't connect to MQTT broker\n");
                    DisconnectNetwork(&network);
                    printf("Connection to MQTT broker dropped. Re-establishing...\n");
                    vTaskDelay(10000/portTICK_RATE_MS);
                }
		}
	}
	}
}


void user_init(void)
{
	wifi_station_set_auto_connect(FALSE);

	mqttControlQueue = xQueueCreate(5,sizeof(taskSignal_t));
	mqttRxQueue = xQueueCreate(5,sizeof(MessageData *));

	xTaskCreate(taskMain,"Main",256,NULL,(tskIDLE_PRIORITY + 2),NULL);
	xTaskCreate(taskMqtt,"MQTT",1024,NULL,(tskIDLE_PRIORITY + 1),NULL);
}
