/*
 * crouton_dev.c
 *
 *  Created on: 13 Aug 2016
 *      Author: Alexander Vorontsov
 */


#include "crouton_dev.h"


//External functions

// Appends base topic with additive string (subtopic)
// Results in "base" + "/" + "additive"
// Both base and additive strings should be null-terminated
// Note! Don't forget to free() the memory if string not used anymore or char* variable re-used.
// Otherwise will cause memory leak
//
// Returns pointer to allocated string of NULL in case of failure
inline char* croutonAppendTopic (char *base, char *additive)
{
	char *result = (char*)malloc(strlen(base)+sizeof("/")+strlen(additive)+1); //Don't forget null-terminator
	if ((base != NULL)&&(additive != NULL)&&(result!=NULL))
		{
			strcpy(result,base);
			strcat(result,"/");
			strcat(result,additive);
		}
	return result;
}

// Send deviceInfo to MQTT broker
croutonStatus croutonSendDevInfo(croutonDevHandle_t *device, MQTTClient *client)
{
		croutonStatus status=CROUTON_NOK;
		//Define outbox/deviceInfo Topic
		char *devInfoOutbox = croutonAppendTopic(device->outboxTopic,"deviceInfo");

    	//Compile message
    	MQTTMessage message;
    	char *devInfo = cJSON_Print(device->deviceRoot);
    	message.payload = (void*)devInfo;
    	message.payloadlen = strlen(devInfo);
    	message.dup = 0;
    	message.qos = QOS0;
    	message.retained = 0;

    	if (MQTTPublish(client, devInfoOutbox, &message)==SUCCESS)
    		{
    			printf("Sending message: %s\n",message.payload);
    			status = CROUTON_OK;
    		}
    	free(devInfoOutbox);
    	free(devInfo);
	return status;
}

croutonDevStatus croutonDeviceCreate (croutonDevHandle_t *device, char *name, char *description, char *status)
{
	if ((device->deviceRoot = cJSON_CreateObject()))
	{
		cJSON_AddItemToObject(device->deviceRoot, "deviceInfo", device->deviceInfo = cJSON_CreateObject());
		cJSON_AddItemToObject(device->deviceInfo, "name", cJSON_CreateString(name));
		cJSON_AddItemToObject(device->deviceInfo, "endPoints", device->deviceEndpoints = cJSON_CreateObject());
		cJSON_AddItemToObject(device->deviceInfo, "description", cJSON_CreateString(description));
		cJSON_AddItemToObject(device->deviceInfo, "status", cJSON_CreateString(status));

		//Fill-in inbox/outbox topics
		if ((device->inboxTopic = croutonAppendTopic("/inbox",name)))
			if ((device->outboxTopic = croutonAppendTopic("/outbox",name))) return DEVICE_CREATE_OK;
	}
	return DEVICE_CREATE_ERR;
}

croutonDevStatus croutonEndpointAdd ( 	croutonEndpointHandle_t	*endpointHandler, \
										croutonDevHandle_t 		*device,\
		                                char *endpointName, char *title, char *units,\
										croutonEndPointCardType_t type)
{
	endpointHandler->device = device;
	cJSON *endPoint;
	cJSON_AddItemToObject(device->deviceEndpoints, endpointName, endPoint = cJSON_CreateObject());

	endpointHandler->type = type;
	switch (type)
	{
		case ENDPOINT_SIMPLE_TEXT:
		{
			cJSON_AddItemToObject(endPoint, "card-type", cJSON_CreateString("crouton-simple-text"));
			break;
		}
		case ENDPOINT_SIMPLE_INPUT:
		{
			cJSON_AddItemToObject(endPoint, "card-type", cJSON_CreateString("crouton-simple-input"));
			break;
		}
	}

	if (title != "") cJSON_AddItemToObject(endPoint, "title", cJSON_CreateString(title));
	if (units != "") cJSON_AddItemToObject(endPoint, "units", cJSON_CreateString(units));

	//Just for testing. Will be changed to appropriate dynamic content
	cJSON *values;
	cJSON_AddItemToObject(endPoint, "values", values = cJSON_CreateObject());
	cJSON_AddNumberToObject(values, "value", 777);

	return DEVICE_CREATE_OK;
}

