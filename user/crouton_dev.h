/*
 * crouton_dev.h
 *
 *  Created on: 13 Aug 2016
 *      Author: Alexander Vorontsov
 */

#ifndef USER_CROUTON_DEV_H_
#define USER_CROUTON_DEV_H_

#include <espressif/esp_common.h>

#include <json/cJSON.h>

#include "MQTTESP8266.h"
#include "MQTTClient.h"
#include "MQTTPacket.h"


typedef enum
{
	DEVICE_CREATE_OK = 1,
	DEVICE_CREATE_ERR = -1,
	DEVICE_CONNECTED = 2,
	DEVICE_DISCONNECTED = -2
} croutonDevStatus;

typedef enum
{
	CROUTON_OK = 1,
	CROUTON_NOK = -1
} croutonStatus;

typedef struct
{
	cJSON *deviceRoot; 		// Root for JSON object
	cJSON *deviceInfo; 		// Link to "deivceInfo" object
	cJSON *deviceEndpoints;	// Link to "endPoints" object
	char *inboxTopic;
	char *outboxTopic;
	croutonDevStatus deviceStatus;
} croutonDevHandle_t;

typedef enum
{
	ENDPOINT_SIMPLE_TEXT,
	ENDPOINT_SIMPLE_INPUT
} croutonEndPointCardType_t;

typedef struct
{
	croutonDevHandle_t *device; //Link to parent device
	cJSON *endPoint; // Link to own JSON object
	cJSON *values; // Link to child "values" JSON object
	croutonEndPointCardType_t type;
} croutonEndpointHandle_t;


// External functions
char* croutonAppendTopic (char *base, char* additive);
croutonStatus croutonSendDevInfo(croutonDevHandle_t *device, MQTTClient *client);
croutonDevStatus croutonDeviceCreate (croutonDevHandle_t *device, char *name, char *description, char *status);
croutonDevStatus croutonEndpointAdd ( croutonEndpointHandle_t *endpointHandler, \
											croutonDevHandle_t *device,\
		                                    char *endpointName, char *title, char *units,\
											croutonEndPointCardType_t type);
#endif /* USER_CROUTON_DEV_H_ */
