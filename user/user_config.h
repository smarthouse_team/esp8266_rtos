/*
 * user_config.h
 *
 *  Created on: 7 Aug 2016
 *      Author: Alexander Vorontsov
 */

#ifndef USER_USER_CONFIG_H_
#define USER_USER_CONFIG_H_

// Includes




// WIFI connection parameters
#define WIFI_CLIENTSSID 		"test"
#define WIFI_CLIENTPASSWORD 	"test1"

// MQTT connection parameters
#define MQTT_HOST "gproject.in.ua" // Hostname or IP-address
#define MQTT_CLIENT_ID "RzPD60fSHx1" //Any unique name
#define MQTT_PORT 1883
#define MQTT_USER ""
#define MQTT_PASS ""
#define MQTT_RXBUF_LEN	512 //Change this parameter with care. Crouton JSON structures require long  buffers
#define MQTT_TXBUF_LEN	512 //Change this parameter with care. Crouton JSON structures require long  buffers

// Dashboard connectivity parameters
#define DASHBRD_DEVNAME "ESP8266"
#define DASHBRD_DEVDESCR "ESP test device"


#endif /* USER_USER_CONFIG_H_ */
